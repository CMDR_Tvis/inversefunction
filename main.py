from typing import Optional, List

from sympy import parse_expr, Eq, Expr, integrate, solve, simplify, Q, trigsimp, Symbol, powsimp
from sympy.abc import u, alpha, xi
from sympy.assumptions.assume import global_assumptions

# Defining f(u)
inp: str = input('f_η(u) := ')
f_η_from_u: Expr = parse_expr(inp)

# Making some assumptions
global_assumptions.add(Q.positive(alpha))
global_assumptions.add(Q.positive(xi))

# Defining a, b
inp = input("a := ")
a: Expr = parse_expr(inp)
inp = input("b := ")
b: Expr = parse_expr(inp)

# Checking norm
# Checking if ∫[a, b](f_η(u) du) = 1
norm: Expr = simplify(integrate(f_η_from_u, (u, a, b)))
print(f"Norm = {str(norm)}")
assert norm == 1

# Solving α_0 = ∫[a, ξ_0](f_η(u) du)
eq: Eq = Eq(powsimp(trigsimp(simplify(integrate(f_η_from_u, (u, a, xi))))), alpha)

print(f"{eq.lhs} = {eq.rhs}")
solutions: List[Expr] = solve(eq, xi)
print(f"ξ_0 = {solutions}")
modeling_formula: Optional[Expr] = None

# Looking for modeling formula amongst solutions

for root in solutions:
    if root.is_complex:
        continue

    if root.subs(alpha, 0) == a and root.subs(alpha, 1) == b:
        modeling_formula = root
        break

print(f"New modeling formula is ξ_0 = {str(modeling_formula)}")
